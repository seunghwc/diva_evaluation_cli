SimSA5_person_reads_document

Description:  A person reading a hand-held document, such as a book, newspaper, flyer, brochure, etc, which they are holding.  These documents should not include digital documents read on a phone, ipad, laptop, etc.  Reading will only be annotated when the reading material is visible and the only participating track is for the person (i.e., the document will not be independently tracked).  The person may look away from the read document for periods of time as part of the same activity as long as it is clear they have not abandoned reading (i.e. the reading is briefly interrupted). In the same vein, if the reading person is occluded, as long as the time period is reasonable as a brief interruption this is part of the same activity.  Flipping through a magazine or book for the minimum duration is considered reading a document.

Start:  Annotation begins at the start of when the person’s attention is focused on the document for a minimum of 5 seconds, without interruption.

End:  The activity ends either ACTIVELY or PASSIVELY. The activity ends actively if the person moves in such a way that stops the reading activity, i.e. closes the book, or stands up, or puts the book down and starts talking to somebody, etc. The activity ends passively if the reader's attention drifts away from the document for more than five seconds.

Minimum Duration:  5 seconds continuous, uninterrupted reading.
