#!/bin/bash

sudo apt-get update

# Fix "sudo: unable to resolve host: umd-diva"
sudo true 2>&1 | grep unable
if [ $? -eq 0 ];then
  echo -e "127.0.0.1 $(cat /etc/hostname)\n$(cat /etc/hosts)" | sudo tee /etc/hosts
fi

# Install ffmpeg
ffmpeg -h
if [ $? -ne 0  ];then
  sudo apt-get install ffmpeg -y
fi

# Install curl
curl -h
if [ $? -ne 0  ];then
  sudo apt-get install curl -y
fi

# -----------------------------------
# Install docker image (TSM Surprise Activity)
# -----------------------------------
sudo docker image ls --filter=reference='umd_cmu_diva_phase3_tsm_surprise' --format='{{.ID}}' | grep .
#sudo docker image ls --filter=reference='umd_cmu_diva_phase3_i3d_surprise' --format='{{.ID}}' | grep .
if [ $? -ne 0 ]; then
  # download the docker image (umd_cmu_diva_phase2_tsm.tar)
  # TSM training
  # curl -J -O -L https://cmu.box.com/shared/static/qv23kicynk321ip3nsh9kk12uf7gr5c4.tar

  # TSM surprise combined
  # curl -J -O -L https://cmu.box.com/shared/static/7tyi45buzoii43r6zteerc1kdrsdew20.tar
  #curl -o /home/ubuntu/umd_cmu_diva_phase3_tsm_surprise_wo_car.tar -J -O -L https://cmu.box.com/shared/static/5svn86dy1xytkt3i4o89x4wgv7nr0jic.tar
  curl -o /home/ubuntu/umd_cmu_diva_phase3_tsm_surprise.tar -J -O -L https://cmu.box.com/shared/static/bfnbq75yy8bfu1cg0t7bea1ears6lnkp.tar
  #curl -o /home/ubuntu/umd_cmu_diva_phase3_tsm_surprise_rgb.tar -J -O -L https://cmu.box.com/shared/static/8djuicq83239no7yhoftq86qy3djmwhj.tar

  # I3D surprise
  #curl -o /home/ubuntu/umd_cmu_diva_phase3_i3d_surprise.tar -J -O -L https://cmu.box.com/shared/static/z3xdc8l1il9rs8f8uge7j0a31643ch4h.tar

  if [ $? -ne 0 ];then
    exit 1
  fi
  #docker pull tea1528/cmu_diva
  #sudo docker load -i /home/ubuntu/umd_cmu_diva_phase3_i3d_surprise.tar
  sudo docker load -i /home/ubuntu/umd_cmu_diva_phase3_tsm_surprise.tar
  if [ $? -ne 0 ];then
    exit 1
  fi
  DOCKER_IMAGE_ID=$(sudo docker image ls --format "{{.ID}}" | head -1)
  sudo docker tag $DOCKER_IMAGE_ID umd_cmu_diva_phase3_tsm_surprise
  sudo docker image ls --filter=reference='umd_cmu_diva_phase3_tsm_surprise' --format='{{.ID}}' | grep .
  if [ $? -ne 0 ];then
    exit 1
  fi
fi
sudo docker inspect -f '{{.Config.Entrypoint}}' umd_cmu_diva_phase3_tsm_surprise
# -----------------------------------
# End 
# -----------------------------------

# -----------------------------------
# Install docker image (TSM)
# -----------------------------------
# sudo docker image ls --filter=reference='umd_cmu_diva_phase2_tsm' --format='{{.ID}}' | grep .
# if [ $? -ne 0 ]; then
#   # download the docker image (umd_cmu_diva_phase2_tsm.tar)
  
#   # no chunking
#   # curl -J -O -L https://cmu.box.com/shared/static/desesuu8vbsvxaji723q3xlizw4sjoxq.tar
  
#   # chunking = best
#   # curl -J -O -L https://cmu.box.com/shared/static/7vo8t7kchattmkh77ponjvrlvxkc44h9.tar

#   # chunking with new scenes ignored
#   # curl -J -O -L https://cmu.box.com/shared/static/ye14nwql4tw02y36i66bab2pzwbitl00.tar

#   # chunking = higher num workers and batch scale = 196
#   curl -J -O -L https://cmu.box.com/shared/static/iv22qsd279x8uj3ykidlrq7m2xy07x8p.tar

#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   sudo docker load -i /home/ubuntu/umd_cmu_diva_phase2_tsm.tar
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   DOCKER_IMAGE_ID=$(sudo docker image ls --format "{{.ID}}" | head -1)
#   sudo docker tag $DOCKER_IMAGE_ID umd_cmu_diva_phase2_tsm
#   sudo docker image ls --filter=reference='umd_cmu_diva_phase2_tsm' --format='{{.ID}}' | grep .
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
# fi
# -----------------------------------
# End 
# -----------------------------------

# -----------------------------------
# Install docker image (Min_SlowFast_TSM)
# -----------------------------------
# sudo docker image ls --filter=reference='umd_cmu_diva_phase2_min_slowfast_tsm' --format='{{.ID}}' | grep .
# if [ $? -ne 0 ]; then
#   # download the docker image (umd_diva_docker_image.tar)
#   curl -J -O -L https://cmu.box.com/shared/static/2qqtk8tf9q1254hy1ijimzbtrd7ys5rs.tar
#   # download code omitted
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   sudo docker load -i /home/ubuntu/umd_cmu_diva_phase2_min_slowfast_tsm.tar
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   DOCKER_IMAGE_ID=$(sudo docker image ls --format "{{.ID}}" | head -1)
#   sudo docker tag $DOCKER_IMAGE_ID umd_cmu_diva_phase2_min_slowfast_tsm
#   sudo docker image ls --filter=reference='umd_cmu_diva_phase2_min_slowfast_tsm' --format='{{.ID}}' | grep .
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
# fi
# -----------------------------------
# End 
# -----------------------------------

# -----------------------------------
# Install docker image (SlowFast)
# -----------------------------------
# sudo docker image ls --filter=reference='umd_cmu_diva_phase2_slowfast' --format='{{.ID}}' | grep .
# if [ $? -ne 0 ]; then
#   # download the docker image (umd_diva_docker_image.tar)
#   curl -J -O -L https://cmu.box.com/shared/static/sg5m2uez0zyrgawxjo58fb0118573yqx.tar
#   # download code omitted
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   sudo docker load -i /home/ubuntu/umd_cmu_diva_phase2_slowfast.tar
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   DOCKER_IMAGE_ID=$(sudo docker image ls --format "{{.ID}}" | head -1)
#   sudo docker tag $DOCKER_IMAGE_ID umd_cmu_diva_phase2_slowfast
#   sudo docker image ls --filter=reference='umd_cmu_diva_phase2_slowfast' --format='{{.ID}}' | grep .
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
# fi
# -----------------------------------
# End 
# -----------------------------------

# -----------------------------------
# Install docker image (SlowFast-TSM)
# -----------------------------------
# sudo docker image ls --filter=reference='umd_cmu_diva_phase2_slowfast_tsm' --format='{{.ID}}' | grep .
# if [ $? -ne 0 ]; then
#   # download the docker image (umd_diva_docker_image.tar)
#   curl -J -O -L https://cmu.box.com/shared/static/dqb3euwjivh2k2hdvzutreuv9av31bv1.tar
#   ## download code omitted
#   #if [ $? -ne 0 ];then
#   #  exit 1
#   #fi
#   sudo docker load -i /home/ubuntu/umd_cmu_diva_phase2_slowfast_tsm.tar
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
#   DOCKER_IMAGE_ID=$(sudo docker image ls --format "{{.ID}}" | head -1)
#   sudo docker tag $DOCKER_IMAGE_ID umd_cmu_diva_phase2_slowfast_tsm
#   sudo docker image ls --filter=reference='umd_cmu_diva_phase2_slowfast_tsm' --format='{{.ID}}' | grep .
#   if [ $? -ne 0 ];then
#     exit 1
#   fi
# fi
# -----------------------------------
# End
# -----------------------------------

# Increase /dev/shm size
tail /etc/fstab -n 1 | grep \/dev\/shm
if [ $? -ne 0 ]; then
  echo 'tmpfs /dev/shm tmpfs defaults,noexec,nosuid,size=126g 0 0' | sudo tee -a /etc/fstab
  sudo mount -o remount /dev/shm
  if [ $? -ne 0 ];then
    exit 1
  fi
fi

