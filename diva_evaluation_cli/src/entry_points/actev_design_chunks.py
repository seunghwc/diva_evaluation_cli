"""Entry point module: design-chunks

Implements the entry-point by using Python or any other languages.
"""

import argparse
import json
import pickle



def entry_point(file_index, activity_index, output, nb_videos_per_chunk):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Given a file index and an activity index, produce a chunks file that is
    suitable for the system.

    Args:
        file_index (str): Path to file index json file for test set
        activity_index (str): Path to activity index json file for test set
        output (str): Path to save chunks file
        nb_video_per_chunk (int): Number of videos in the chunk

    """
    MIN_VIDEOS_PER_CHUNK = 32
    MAX_VIDEOS_PER_CHUNK = 32

    with open(file_index) as fin:
        files = [k for k in json.load(fin)]
    with open(activity_index) as fin:
        activities = [k for k in json.load(fin)]

    chunks = dict()
    if nb_videos_per_chunk is None:
        videos_per_chunk = MAX_VIDEOS_PER_CHUNK
    else:
        videos_per_chunk = max(MIN_VIDEOS_PER_CHUNK, min(MAX_VIDEOS_PER_CHUNK, int(nb_videos_per_chunk)))

    start_index = 0
    chunk_id = 1
    while start_index < len(files):
        end_index = min(len(files), start_index + videos_per_chunk)
        chunks['Chunk{}'.format(chunk_id)] = {
            'activities': activities,
            'files': files[start_index:end_index]
        }
        chunk_id += 1
        start_index += videos_per_chunk

    with open(output, 'w') as fout:
        json.dump(chunks, fout, indent=2)

    # act_idx_to_name = {i + 1: k for i, k in enumerate(activities)}
    # with open(os.path.join(os.path.dirname(output), "act_idx_to_name.pkl"), "wb") as f:
    #     pickle.dump(act_idx_to_name, f, protocol=pickle.HIGHEST_PROTOCOL)
