SimSA5_vehicle_turns_right

Description:  A vehicle turning left or right is determined from the POV of the driver of the vehicle.  The vehicle may not stop for more than 10 s during the turn.  This event is determined after a reasonable interpretation of the video.

Start:  Annotation begins 1 s before vehicle has noticeably changed direction.

End:  Annotation ends 1 s after the vehicle is no longer changing direction and linear motion has resumed.
