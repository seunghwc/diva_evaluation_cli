# actev post-process-chunk

## Description

Post process a chunk

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| chunk-id            | i | True    | chunk id                         |
| system-cache-dir    | s | True    | path to system cache directory   |

## Usage

Generic command:

```bash
actev post-process-chunk -i <id of a chunk> -s <path to system cache directory>
```

Example:

```bash
actev post-process-chunk -i Chunk1 -s ~/system_cache_dir
```
