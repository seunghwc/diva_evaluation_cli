
import json
import jsonschema
import os
import sys

if len(sys.argv) != 2:
    print("usage: python3 validate_localization.py <path_to_localization_file>")
    sys.exit(1)


def load_json(path):
    with open(path, 'r') as fd:
        return json.load(fd)


schema = load_json(os.path.join(os.getcwd(), 'CLI_localization_schema.json'))
localization = load_json(sys.argv[1])
try:
    jsonschema.validate(localization, schema)
    print('ok')
except Exception as e:
    raise e
