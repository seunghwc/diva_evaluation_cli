"""Entry point module: post-process-chunk

Implements the entry-point by using Python or any other languages.
"""

import os


def entry_point(chunk_id, system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Post-process a chunk.

    Args:
        chunk_id (int): Chunk id
        system_cache_dir (str): Path to system cache directory

    """
    pass
