# actev experiment-init

## Description

Performs any operation that prepares the execution of the system such as booting containers, etc...

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|----------|----------------------------|
| file-index              | f | True    | path to file index json file       |
| activity-index          | a | True    | path to activity index json file   |
| chunks                  | c | True    | path to chunks json file           |
| nb-videos-per-chunk      | n | True   | number of videos in the chunk      |
| video-location          | v | True    | path to videos content             |
| system-cache-dir        | s | True    | path to system cache directory     |
| config-file             | C | False   | path to config file                |
| prepare_proposal_outputs | :x: | False | If True, the system will retain proposal output information during execution and output the proposals during merge_chunks |
| prepare_localization_outputs | :x: | False | If True, the system will retain spatial localization output information during execution and output the proposals during merge_chunks |

## Usage

Generic command:

```bash
actev experiment-init -f <path to file.json> -a <path to activity.json> -c <path to chunks.json > -v <path to videos directory> -s <path to system cache directory>
```

Example:

```bash
actev experiment-init -f ~/file.json -a ~/activity.json -c ~/chunks.json -v ~/videos/ -s ~/system_cache_dir/
```
