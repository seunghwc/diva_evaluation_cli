"""Entry point module: system-setup

Implements the entry-point by using Python or any other languages.
"""

import os

from diva_evaluation_cli.bin.private_src.implementation.utils.\
    actev_cli_environment import export_variable, activate_environment


def entry_point(dev=False):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Run any compilation/preparation steps for the system.

    Args:
        dev (bool, optional): Turn on development mode (no monitoring/logging)
            if set
    """
    # Do not remove these two lines.
    export_variable('ACTEV_SYSTEM_SETUP_DEV', 1 if dev else 0)
    activate_environment()

    # Performer code below

    # go into the right directory to execute the script
    path = os.path.dirname(__file__)
    script = os.path.join(path, 'setup.sh')

    # execute the script
    # status is the exit status code returned by the program
    status = os.system(script)
    if status != 0:
        raise Exception("Error occured in setup.sh")
