# ActEV CLI README

## Introduction

Consult the documentation to learn more about the CLI: [introduction](doc/introduction.md)

This repository contains:

* an abstract CLI to implement on the `master` branch.
* an implementation example on the branch `baseline_system_master`.

You should fork the project and use the `master` branch in order to implement the entry point methods and get the updates.

## Test it

### Get the CLI

**Requirements:** python > 3.7

Clone the repository:

```bash
git clone https://gitlab.kitware.com/actev/diva_evaluation_cli.git
```

### Install it

* Go into the clone of this repository:

```bash
cd diva_evaluation_cli
```

* Execute the following script:

```bash
diva_evaluation_cli/bin/install.sh
```

See `diva_evaluation_cli/bin/install.sh -h` for more options. If using `--all`, make sure to install the submodules before.

N.B.: We recommend to set the `-c` and `-s` options to what will be used as `system_cache_dir` by the entry_points ; otherwise, some operations like `validate-execution` will likely fail.

### Test the installation

Run the following command:

```bash
actev
```

You should be able to see the available subcommands.

## Usage

### Command line

Run the following command to obtain help with the CLI:

```bash
actev -h
```

### Documentation

Consult the documentation to have information: [CLI](doc/cli_commands/index.md)

## Fork it and develop your own implementation

### Fork it

Click on the “Fork” button to make a copy of the repository in your own space and add this repository as a remote upstream to get the latest updates

```bash
git remote add upstream https://gitlab.kitware.com/actev/diva_evaluation_cli.git
```

### Update it

Execute the following commands to update it:

```bash
git fetch upstream
git checkout master
git rebase upstream/master
```

### Develop it

The CLI components are included in the `bin` directory, you do not have to modify it.
To add your code, you simply have to implement the methods in `src/entry_points`.

We suggest you to call your scripts in the entry point methods and store them in `src`.
More information about the development and the update of the CLI here: [development](doc/development.md)

:warning: Since release 1.2.4, we made sure the content of `/bin` from **your** CLI will be ignored. For example, if you used to edit `/bin/install.sh`, your submissions will likely fail from now. The best way is to move this kind of files/scripts into entry-points `actev-experiment-init` and/or `actev-system-setup`.

:warning: Since release 1.2.4, the CLI installation supports [conda](https://docs.conda.io/en/latest/) environments. This is now the default way for installing the CLI dependencies, **however if you wish to stick with `pip`, you will have to remove `environment.yml` from your CLI.

:warning: Since release 1.2.4, the entry-point `validate-system` requires the existence of a `LICENSE.txt` file at the root of your project.

## Contact

* diva-nist@nist.gov
