"""Entry point module: merge-chunks

Implements the entry-point by using Python or any other languages.
"""
import os
import argparse
import json


def entry_point(system_cache_dir, output_file, chunks_file, chunk_ids,
                proposals=None, localization=None):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Given a list of chunk ids, merges all the chunks system output present in
    the list.

    Args:
        result_location (str): Path to get the result of the chunks processing
        chunk_file (str):  Path to generate a chunk file, that summarizes all
            the processed videos/activities
        output_file (str): Path to the output file generated
        chunk_ids (:obj:`list`): List of chunk ids
        system_cache_dir (str): Path to system cache directory
        proposals (str): path to generate a proposals file, that contains the
            system proposals
        localization (str): path to generate a localization file
    """
    system_cache_dir = result_location

    # despite what the doc string says, the chunks_ids argument is actually None
    with open(os.path.join(system_cache_dir, 'chunks.json')) as fin:
        chunk_ids = list(json.load(fin).keys())

    output_dict = {"filesProcessed": [], "activities": []}
    chunks_dict = {}

    max_activity_id = 1
    activity_id_offset = 0
    for chunk_id in chunk_ids:
        with open(os.path.join(system_cache_dir, chunk_id, 'out', 'ad.json'), 'r') as fin:
            chunk_output = json.load(fin)

        output_dict['filesProcessed'].extend(chunk_output['filesProcessed'])
        # update activity ids so they are unique between chunks
        for act_index in range(len(chunk_output['activities'])):
            old_activity_id = chunk_output['activities'][act_index]['activityID']
            new_activity_id = old_activity_id + activity_id_offset
            max_activity_id = max(new_activity_id, max_activity_id)
            chunk_output['activities'][act_index]['activityID'] = new_activity_id

        chunks_dict[chunk_id] = chunk_output
        output_dict['activities'].extend(chunk_output['activities'])
        activity_id_offset = max_activity_id + 1

    with open(output_file, 'w') as f:
        json.dump(output_dict, f, indent=0)

    with open(chunks_file, 'w') as f:
        json.dump(chunks_dict, f, indent=0)
