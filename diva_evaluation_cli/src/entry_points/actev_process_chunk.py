"""Entry point module: process-chunk

Implements the entry-point by using Python or any other languages.
"""

import os
import re
import json
import shutil



def entry_point(chunk_id, system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Process a chunk.

    Args:
        chunk_id (int): Chunk id
        system_cache_dir (str): Path to system cache directory

    """
    
    print('='*20)
    print('process.py')
    print('='*20)

    # For final submission, use_ram_as_cache = True, use_gpus = 'all'
    use_ram_as_cache = True
    use_gpus = 'all'
    #use_gpus = [1, 3]
    docker_image_name = 'umd_cmu_diva_phase3_tsm_surprise'

    with open(os.path.join(system_cache_dir, 'file_index.json')) as fin:
        file_index = json.load(fin)
    # with open(os.path.join(system_cache_dir, 'activity_index.json')) as fin:
    #     activities = [k for k in json.load(fin)]
    with open(os.path.join(system_cache_dir, 'chunks.json')) as fin:
        chunks = json.load(fin)
    with open(os.path.join(system_cache_dir, 'video_location.json')) as fin:
        video_location_dict = json.load(fin)
        video_location = video_location_dict['video_location']

    print("vid: ", video_location)

    chunk_files = chunks[chunk_id]['files']
    # chunk_activities = chunks[chunk_id]['activities']

    chunk_input_dir = os.path.join(system_cache_dir, chunk_id, 'in')
    chunk_output_dir = os.path.join(system_cache_dir, chunk_id, 'out')
    chunk_metadata_dir = os.path.join(system_cache_dir, chunk_id, 'meta')

    os.makedirs(chunk_input_dir, exist_ok=True)
    os.makedirs(chunk_output_dir, exist_ok=True)
    os.makedirs(chunk_metadata_dir, exist_ok=True)

    os.system('sudo ls -l {}'.format(video_location))
    for root, _, files in os.walk(video_location):
        for f in files:
            print(os.path.join(root, f))

    chunk_meta = dict()
    for idx, video_name in enumerate(sorted(list(chunk_files))):
        print('Video number {}, named {}'.format(idx, video_name))

        # Only needed for the current dir structure
        # Important: Remove this line for submission!
        #vid = os.path.split(video_name)[-1]
        #d = vid.split(".")[0]
        #num = vid.split(".")[2][:2]
        #input_fname = os.path.join(video_location, d, num, vid)
        input_fname = os.path.join(video_location, video_name)

        output_dir = os.path.join(chunk_input_dir, '{:04d}'.format(idx))
        if video_name in file_index:
            chunk_meta[video_name] = file_index[video_name]
            if 'filename' in file_index[video_name]:
                input_fname = os.path.join(video_location, file_index[video_name]['filename'])
            if 'camera_id' in file_index[video_name]:
                output_dir = os.path.join(chunk_input_dir, file_index[video_name]['camera_id'])
        os.makedirs(output_dir, exist_ok=True)
        output_fname = os.path.join(output_dir, os.path.split(video_name)[-1])

        if not os.path.exists(input_fname):
            print("{} doesn't exists".format(input_fname))

        shutil.copy(input_fname, output_fname, follow_symlinks=True)
        if not os.path.exists(output_fname):
            print('{} doesnt exist, trying to force a copy'.format(output_fname))
            os.system('sudo mkdir -p {}'.format(output_dir))
            os.system('sudo cp -fv {} {}'.format(input_fname, output_fname))
            if not os.path.exists(output_fname):
                print('{} still doesnt exist, giving up'.format(output_fname))
                raise IOError('{} still doesnt exist, giving up'.format(output_fname))
        else:
            print('{} exists, should be good'.format(output_fname))

    with open(os.path.join(chunk_metadata_dir, 'file_index.json'), 'w') as fout:
        json.dump(chunk_meta, fout, indent=0)

    # copy activity index to meta dir
    cur_path = os.path.dirname(os.path.realpath(__file__))

    # shutil.copy(os.path.join(system_cache_dir, "act_idx_to_name.pkl"), os.path.join(chunk_metadata_dir, "act_idx_to_name.pkl"))
    shutil.copy(os.path.join(cur_path, "data", "meta", "act_idx_to_name.pkl"), os.path.join(chunk_metadata_dir, "act_idx_to_name.pkl"))

    # If /dev/shm is not the workspace dir (last but one argument in docker run i.e. one before out_dir), then do 777 on the workspace dir
    if not use_ram_as_cache:
        os.system('chmod -R 777 /home/ubuntu/system_cache_dir_inference')
        script_cache_str = '-v /home/ubuntu/system_cache_dir_inference/:/workspace'
    else:
        script_cache_str = ''

    script_cmd_str = 'sudo docker run'
    if isinstance(use_gpus, str) and use_gpus.upper() == 'ALL':
        script_gpu_str = '--gpus all'
    else:
        # e.g. --gpus \'"device=0,1"\'
        script_gpu_str = r"""--gpus '"device=""" + ''.join([str(g)+',' for g in use_gpus])[:-1] + r""""'"""

    # Path to extracted features
    train_dir_orig = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "out")
    
    print("{} exists: {}".format(train_dir_orig, os.path.exists(train_dir_orig)))
    # train_dir = "/train_workspace"
    
    # shutil.copytree(train_dir_orig, os.path.join(system_cache_dir, "train_workspace"))

    # misc_str = '--skip-flow --skip-detections --skip-database --skip-clustering --skip-jittering --skip-preprocessing --skip-evaluation'
    # misc_str = ''
    script_dir_str = '--shm-size 126g -v {}:/meta -v {}:/in_dir -v {}:/out_dir -v {}:/train_workspace'.format(chunk_metadata_dir, chunk_input_dir, chunk_output_dir, train_dir_orig)

    #script_out_retrieval_str = '--out-retrieval /train_workspace --conf-threshold 0.2 --batch-size 99 --num-gpus 3 --skip-flow'
    script_out_retrieval_str = '--out-retrieval /train_workspace --conf-threshold 0.2 --batch-size 195 --num-gpus 3'

    if not use_ram_as_cache:
        script_metadir_str = '--metadata-dir /meta /in_dir /workspace /out_dir'
    else:
        script_metadir_str = '--metadata-dir /meta /in_dir /dev/shm /out_dir'

    script = ' '.join([script_cmd_str, script_gpu_str, script_dir_str, script_cache_str, docker_image_name, script_out_retrieval_str, script_metadir_str])

    print(script)
    
    # give ownership to the docker user so it can write to output volumn
    os.system('sudo chown 16130 {dir_name} -R && sudo chgrp 16130 {dir_name} -R'.format(dir_name=chunk_input_dir))
    os.system('sudo chown 16130 {dir_name} -R && sudo chgrp 16130 {dir_name} -R'.format(dir_name=chunk_output_dir))
    os.system('sudo chown 16130 {dir_name} -R && sudo chgrp 16130 {dir_name} -R'.format(dir_name=chunk_metadata_dir))
    print(chunk_input_dir, 'contains...')
    os.system('sudo ls -l {}'.format(chunk_input_dir))
    print('Running docker...')
    print(script)
    os.system(script)
    os.system('sudo chown ubuntu {dir_name} -R && sudo chgrp ubuntu {dir_name} -R'.format(dir_name=chunk_metadata_dir))
    os.system('sudo chown ubuntu {dir_name} -R && sudo chgrp ubuntu {dir_name} -R'.format(dir_name=chunk_output_dir))
    os.system('sudo chown ubuntu {dir_name} -R && sudo chgrp ubuntu {dir_name} -R'.format(dir_name=chunk_input_dir))

    if not os.path.exists(os.path.join(chunk_output_dir, 'ad.json')):
        raise Exception("Error occured during run, output file \"{}\" not present".format(
            os.path.join(chunk_output_dir, 'ad.json')))


