#!/bin/bash
# git submodule init
# git submodule update

CONTAINER_OUTPUT=/home/ubuntu/Sean/diva_evaluation_cli/diva_evaluation_cli/container_output/ActEV-Eval-CLI-Validation-Set7_UMCMU
mkdir -p ${CONTAINER_OUTPUT}
cp /home/ubuntu/system_output/ActEV-Eval-CLI-Validation-Set7_* $CONTAINER_OUTPUT
cp /home/ubuntu/system_cache_dir_inference/file_index.json ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_file.json
cp /home/ubuntu/system_cache_dir_inference/activity_index.json ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_activity.json
cp ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_output.json ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_UMCMU_output.json

actev validate-execution \
  -o ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_output.json \
  -r ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_output.json \
  -a ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_activity.json \
  -f ${CONTAINER_OUTPUT}/ActEV-Eval-CLI-Validation-Set7_file.json
