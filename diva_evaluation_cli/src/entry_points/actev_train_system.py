"""Entry point module: train-system

Implements the entry-point by using Python or any other languages.
"""
import os
import json
import sys
import subprocess
try:
    import yaml
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pyyaml'])
try:
    import cv2
except ImportError:
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'opencv-python'])
finally:
    import cv2
import shutil
import pickle
pickle.HIGHEST_PROTOCOL = 4
import csv


def entry_point(activity_index, training_data_dir, verbose=False):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Train your system on an unknown activity. The command will read the
    contents of the activity index and train the system for the new activities.
    Data referenced to the activity-index is relative to the training data
    directory.

    Args:
        activity_index (str): Path to activity index json file
        training_data_dir (str): Path to training data directory
    """
    # Content of a training dir:
    # training/
    # +-- SimSA5_*/
    #   +-- SimSA5_*.txt
    #   +-- ex00*/
    #     +-- symbolic link to video
    #     +-- activities.yml
    #     +-- types.yml
    #     +-- geom.yml
    def json_load(path):
        with open(path, 'r') as fd:
            data = json.load(fd)
        return data

    def json_save(path, data):
        with open(path, 'w') as fd:
            json.dump(data, fd)

    def yml_load(path):
        with open(path, 'r') as fd:
            # data = yaml.load(fd, Loader=yaml.FullLoader)
            data = yaml.load(fd)
        return data

    def log(msg):
        # if verbose:
        print("[Training] %s" % (msg))

    def read_ai(path):
        log("Reading activity-index.json")
        ai = json_load(path)
        acts = {'known': [], 'surprise': []}
        for activity in ai:
            if ai[activity].get('training', None) is None:
                acts['known'].append(activity)
            else:
                acts['surprise'].append(activity)
        log("Found %d known activities" % (len(acts['known'])))
        log("Found %d surprise activities" % (len(acts['surprise'])))
        return ai, acts

    def read_examplars(path, act, activities):
        cur_path = os.path.dirname(os.path.realpath(__file__))

        chunk_metadata_dir = os.path.join(cur_path, "data", "meta")
        chunk_input_dir = os.path.join(cur_path, "data", "in")
        chunk_out_dir = os.path.join(cur_path, "data", "out")
        annotation_dir = os.path.join(chunk_metadata_dir, "annotation")

        os.makedirs(chunk_input_dir, exist_ok=True)
        os.makedirs(chunk_metadata_dir, exist_ok=True)
        os.makedirs(chunk_out_dir, exist_ok=True)
        os.makedirs(annotation_dir, exist_ok=True)

        l = {}
        for a in activities["surprise"]:
            for an in act[a]['training']['examplars']:
                vid = an["filename"].split("/")[-1]
                vid_dir = ".".join(vid.split(".")[:-1]) # Remove extension
            
                an_dir = os.path.join(path, *an["filename"].split("/")[:-1])

                # Copy videos to in_dir
                input_fname = os.path.join(an_dir, vid)
                output_dir = os.path.join(chunk_input_dir, a)
                os.makedirs(output_dir, exist_ok=True)
                output_fname = os.path.join(output_dir, vid)
                shutil.copy(input_fname, output_fname, follow_symlinks=True)
                if not os.path.exists(output_fname):
                    log('{} doesnt exist, trying to force a copy'.format(output_fname))
                    os.system('sudo mkdir -p {}'.format(output_dir))
                    os.system('sudo cp -fv {} {}'.format(input_fname, output_fname))
                    if not os.path.exists(output_fname):
                        log('{} still doesnt exist, giving up'.format(output_fname))
                        raise IOError('{} still doesnt exist, giving up'.format(output_fname))
                else:
                    log('{} exists, should be good'.format(output_fname))

                cap = cv2.VideoCapture(input_fname)
                length = "{}".format(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))
                fps = int(cap.get(cv2.CAP_PROP_FPS))
                l[vid] = {"filename": vid, 
                        "framerate": fps, 
                        "camera_type": "EO", 
                        "selected": {'1': 1, length:0},
                        "camera_id": a
                        }
                
                # Load YAML files
                activity = yml_load(os.path.join(path, an["annotation"]["activities"]))
                geom = yml_load(os.path.join(path, an["annotation"]["geom"]))
                types = yml_load(os.path.join(path, an["annotation"]["types"]))
                    
                # Convert to JSON
                full_write_path = os.path.join(annotation_dir, a, vid_dir)
                os.makedirs(full_write_path, exist_ok=True)
                    
                json_save(os.path.join(full_write_path, vid_dir) + '.activities.json', activity)
                json_save(os.path.join(full_write_path, vid_dir) + '.geom.json', geom)
                json_save(os.path.join(full_write_path, vid_dir) + '.types.json', types)

        # file_index.json should be included in the meta dir for running the UMD pipeline
        with open(os.path.join(chunk_metadata_dir, 'file_index.json'), 'w') as fout:
            json.dump(l, fout, indent=0)
            
        act_idx_to_name = {int(idx + 1): a for idx, a in enumerate(activities["surprise"])}
        with open(os.path.join(chunk_metadata_dir, "act_idx_to_name.pkl"), "wb") as f:
            # Store data (serialize)
            pickle.dump(act_idx_to_name, f, protocol=pickle.HIGHEST_PROTOCOL)

        # Needed for surprise act finetuning
        with open(os.path.join(chunk_metadata_dir, 'fully_annotated_vids.csv'), 'w') as csvfile:
            csvwriter = csv.writer(csvfile)  
        
            csvwriter.writerow(['FILENAME'])  
            csvwriter.writerows([[vid[:-4]] for vid in l.keys()])
        
        train_ann_dir = os.path.join(chunk_metadata_dir, "train")
        val_ann_dir = os.path.join(chunk_metadata_dir, "val")

        # Now create train & val
        for act_dir in os.listdir(annotation_dir):
            vids = os.listdir(os.path.join(annotation_dir, act_dir))
            for i, vid_path in enumerate(vids):
                if i == 0:
                    shutil.copytree(os.path.join(annotation_dir, act_dir, vid_path), os.path.join(val_ann_dir, act_dir, vid_path))
                else:
                    shutil.copytree(os.path.join(annotation_dir, act_dir, vid_path), os.path.join(train_ann_dir, act_dir, vid_path))                
            
        log("Finished preparing files for processing")
        

    def run_training(use_ram_as_cache, use_gpus, docker_image_name):
        cur_path = os.path.dirname(os.path.realpath(__file__))

        chunk_metadata_dir = os.path.join(cur_path, "data", "meta")
        chunk_input_dir = os.path.join(cur_path, "data", "in")
        chunk_output_dir = os.path.join(cur_path, "data", "out")

        # If /dev/shm is not the workspace dir (last but one argument in docker run i.e. one before out_dir), then do 777 on the workspace dir
        if not use_ram_as_cache:
            os.system('chmod -R 777 /home/ubuntu/system_cache_dir_training')
            script_cache_str = '-v /home/ubuntu/system_cache_dir_training/:/workspace'
        else:
            script_cache_str = ''

        script_cmd_str = 'sudo docker run'
        if isinstance(use_gpus, str) and use_gpus.upper() == 'ALL':
            script_gpu_str = '--gpus all'
        else:
            # e.g. --gpus \'"device=0,1"\'
            script_gpu_str = r"""--gpus '"device=""" + ''.join([str(g)+',' for g in use_gpus])[:-1] + r""""'"""

        script_dir_str = '--shm-size 126g -v {}:/meta -v {}:/in_dir -v {}:/out_dir'.format(chunk_metadata_dir, chunk_input_dir, chunk_output_dir)

        if not use_ram_as_cache:
            #script_metadir_str = '--is-train --batch-size 100 --skip-flow --num-gpus 3 --metadata-dir /meta /in_dir /workspace /out_dir'
            script_metadir_str = '--is-train --batch-size 180 --num-gpus 3 --metadata-dir /meta /in_dir /workspace /out_dir'
            #script_metadir_str = '--is-train --batch-size 64 --config-file /home/diva/umd_diva/training_pipeline/vijay_train_config.toml --metadata-dir /meta /in_dir /workspace /out_dir'
        else:
            #script_metadir_str = '--is-train --batch-size 100 --skip-flow --num-gpus 3 --metadata-dir /meta /in_dir /dev/shm /out_dir'
            script_metadir_str = '--is-train --batch-size 180 --num-gpus 3 --metadata-dir /meta /in_dir /dev/shm /out_dir'
            #script_metadir_str = '--is-train --batch-size 64 --config-file /home/diva/umd_diva/training_pipeline/vijay_train_config.toml --metadata-dir /meta /in_dir /dev/shm /out_dir'

        script = ' '.join([script_cmd_str, script_gpu_str, script_dir_str, script_cache_str, docker_image_name, script_metadir_str])
        print(script)
        
        # give ownership to the docker user so it can write to output volumn
        os.system('sudo chown 16130 {dir_name} -R && sudo chgrp 16130 {dir_name} -R'.format(dir_name=chunk_input_dir))
        os.system('sudo chown 16130 {dir_name} -R && sudo chgrp 16130 {dir_name} -R'.format(dir_name=chunk_output_dir))
        os.system('sudo chown 16130 {dir_name} -R && sudo chgrp 16130 {dir_name} -R'.format(dir_name=chunk_metadata_dir))
        print(chunk_input_dir, 'contains...')
        os.system('sudo ls -l {}'.format(chunk_input_dir))

        print("Checking gpu driver and docker version...")
        os.system("sudo docker --version")
        os.system("nvidia-smi")

        print('Running docker...')
        print(script)
        os.system(script)
        os.system('sudo chown ubuntu {dir_name} -R && sudo chgrp ubuntu {dir_name} -R'.format(dir_name=chunk_metadata_dir))
        os.system('sudo chown ubuntu {dir_name} -R && sudo chgrp ubuntu {dir_name} -R'.format(dir_name=chunk_output_dir))
        os.system('sudo chown ubuntu {dir_name} -R && sudo chgrp ubuntu {dir_name} -R'.format(dir_name=chunk_input_dir))

        # Copy extracted features and proposals to current path
        #os.makedirs(os.path.join(cur_path, "workspace"), exist_ok=True)

        # Remove train & val
        print('Removing converted annotations')
        os.system('rm -rf {}'.format(os.path.join(chunk_metadata_dir, "train")))
        os.system('rm -rf {}'.format(os.path.join(chunk_metadata_dir, "val")))
        os.system('rm -rf {}'.format(os.path.join(chunk_metadata_dir, "annotation")))

    act, activities = read_ai(os.path.join(training_data_dir, activity_index))
    read_examplars(training_data_dir, act, activities)

    # Now run training
    # For final submission, use_ram_as_cache = True, use_gpus = 'all'
    use_ram_as_cache = True
    use_gpus = 'all'
    # use_gpus = [0, 1]
    docker_image_name = 'umd_cmu_diva_phase3_tsm_surprise'
    #docker_image_name = 'umd_cmu_diva_phase3_i3d_surprise'

    run_training(use_ram_as_cache, use_gpus, docker_image_name)

    # if use_ram_as_cache:
    #     # Before finishing up the training, remove workspace (ex: /dev/shm) so that it won't conflict with actual testing
    #     for dirs in os.listdir("/dev/shm/"):
    #         shutil.rmtree(os.path.join("/dev/shm/", dirs))
    #     shutil.rmtree(chunk_input_dir)
