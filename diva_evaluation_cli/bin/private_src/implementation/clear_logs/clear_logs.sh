#!/bin/bash

CURRENT_DIR=$(dirname $0)/../..

for LOG in `find $CURRENT_DIR -type f | grep "_monitoring.json"`; do
    rm $LOG
    echo "$LOG deleted"
done
