#!/bin/bash

if [[ $# -gt 1 ]]; then
  echo "usage: install.sh [<scorer_conda_env_path>]"
  exit 1
fi

if [[ $# -eq 1 ]];then
  current_path=`realpath $(dirname $0)`
  conda env create -f $current_path/ActEV_Scorer/environment.yml -p $1
  eval "$(conda shell.bash hook)"
  conda activate $1
  conda deactivate
else
  python3 -m pip install -r $current_path/ActEV_Scorer/requirements.txt
fi
