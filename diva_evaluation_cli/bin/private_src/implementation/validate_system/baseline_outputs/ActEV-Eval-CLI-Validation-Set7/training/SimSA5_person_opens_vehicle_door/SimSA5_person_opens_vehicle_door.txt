SimSA5_person_opens_vehicle_door

Description:  A person opening the door to a vehicle.  The only necessary track in this event is the vehicle.  The vehicle door is not independently annotated from the vehicle.  This event often overlaps with entering/exiting; however, can be independent or absent from these events.  The person does not need to be visible for the duration of the activity; however, if a person is visible they will be annotated.  People will be annotated through bus windows when visible.

Start:  The event begins 1 s before the door starts to move.

End:  The event ends as soon as the door stops moving.
