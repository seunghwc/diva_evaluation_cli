"""Entry point module: experiment-init

Implements the entry-point by using Python or any other languages.
"""

import os
import shutil
import json

def entry_point(file_index, activity_index, chunks,
                video_location, system_cache_dir, config_file=None,
                prepare_proposal_outputs=False,
                prepare_localization_outputs=False):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Start servers, starts cluster, etc.

    Args:
        file_index(str): Path to file index json file for test set
        activity_index(str): Path to activity index json file for test set
        chunks (str): Path to chunks json file
        video_location (str): Path to videos content
        system_cache_dir (str): Path to system cache directory
        config_file (str, optional): Path to config file
        prepare_proposal_outputs (str, optional): If True, the system will
            retain proposal output information during execution and output
            the proposals during merge_chunks
        prepare_localization_outputs (str, optional): If True, the system will
            retain spatial localization output information during execution and
            output the proposals during merge_chunks
    """
    print('='*20)
    print('init.py')
    print('='*20)
    shutil.copy(file_index, os.path.join(system_cache_dir, 'file_index.json'))
    shutil.copy(activity_index, os.path.join(system_cache_dir, 'activity_index.json'))
    shutil.copy(chunks, os.path.join(system_cache_dir, 'chunks.json'))

    # shutil.copy(os.path.join(os.path.dirname(chunks), "act_idx_to_name.pkl"), os.path.join(system_cache_dir, 'act_idx_to_name.pkl'))

    video_location_dict = {'video_location': video_location}
    with open(os.path.join(system_cache_dir, 'video_location.json'), 'w') as fout:
        json.dump(video_location_dict, fout, indent=2)
