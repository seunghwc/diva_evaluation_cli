# actev design-chunks

## Description

Given a file index and an activity index, produce a chunks file that is suitable for the system.

The command contains the following subcommands:

## Parameters

| Name      | Id  | Required        | Definition                 |
|-----------|-----|-----------------|----------------------------|
| file-index           | f | True        | path to file index json file      |
| activity-index       | a | True        | path to activity index json file  |
| output               | o | True        | path to save chunks file          |
| nb-videos-per-chunk  | n | True        | number of videos in the chunk     |

## Usage

Generic command:

```bash
actev design-chunks -f <path to file.json> -a <path to activity.json> -o <path to output result> -n <number of video per chunks> 
```

Example:

```bash
actev design-chunks -f ~/file.json -a ~/activity.json -o ~/chunks.json -n 96
```
