#!/bin/bash
mkdir -p /home/ubuntu/system_cache_dir_inference
mkdir -p /home/ubuntu/system_cache_dir_training
mkdir -p /home/ubuntu/system_output

# Add actev train-system
# actev train-system \
# -a /home/ubuntu/tmp/actev-data-repo/partitions/ActEV-Eval-CLI-Validation-Set7_correct/system_input/activity-index.json \
# -t /home/ubuntu/tmp/actev-data-repo/partitions/ActEV-Eval-CLI-Validation-Set7_correct/system_input/ \

actev exec \
-f /home/ubuntu/tmp/actev-data-repo/partitions/ActEV-Eval-CLI-Validation-Set7_correct/system_input/file-index.json \
-a /home/ubuntu/tmp/actev-data-repo/partitions/ActEV-Eval-CLI-Validation-Set7_correct/system_input/activity-index.json \
-c /home/ubuntu/chunks_set7.json \
-n 32 \
-v /home/ubuntu/tmp/actev-data-repo/corpora/MEVA/video/KF1 \
-s /home/ubuntu/system_cache_dir_inference \
-o /home/ubuntu/system_output/ActEV-Eval-CLI-Validation-Set7_output.json \
-r /home/ubuntu/system_output/ActEV-Eval-CLI-Validation-Set7_chunk.json
