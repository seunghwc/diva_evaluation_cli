# actev reset-chunk

## Description

Deletes the files generated when processing a chunk. Ideally to be able to process the chunk again.
This command is not used yet by the pipeline.

The command contains the following subcommands:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| chunk-id            | i | True    | chunk id                         |
| system-cache-dir    | s | True    | path to system cache directory   |

## Usage

Generic command:

```bash
actev reset-chunk -i <id of a chunk> -s <path to system cache directory>
```

Example:

```bash
actev reset-chunk -i Chunk1 -s ~/system_cache_dir
```
